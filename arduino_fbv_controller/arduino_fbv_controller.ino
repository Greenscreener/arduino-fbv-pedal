
#include <SoftwareSerial.h> 


#define LENGTH(x)  (sizeof(x) / sizeof((x)[0]))

// Button IDs
#define FUNC 0x01
#define COMP 0x02
// 0x03 - 0x0F seems to be pedal
#define FX1 0x12
#define FX2 0x41
#define FX3 0x51
#define REV 0x21
#define A 0x20
#define B 0x30
#define C 0x40
#define D 0x50
#define TAP 0x61
#define WAH 0x43

SoftwareSerial ampSerial(3, 2); // RX, TX

struct button {
  int pin;
  byte ID;
  bool lastState;
};

struct button buttons[] = {
  {4, A, false},
  {5, B, false},
  {6, C, false},
  {7, D, false},
  {8, TAP, false}
};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  ampSerial.begin(31250);

  for (int i = 0; i < LENGTH(buttons); i++) {
    pinMode(buttons[i].pin, INPUT_PULLUP);
  }

  // Write start message (idk what it means, copied from https://alan.ferrency.com/2017/10/05/line6-fbv-part-2/)
  byte msg[] = {0xf0, 0x02, 0x90, 0x00, 0xf0, 0x02, 0x30, 0x08};
  ampSerial.write(msg, sizeof(msg));

}


void loop() {
  // put your main code here, to run repeatedly:
  if (ampSerial.available()) {
    byte bajt = ampSerial.read();
    if (bajt == 0xf0) {
      Serial.println();
    }
    Serial.print(bajt,HEX);
    Serial.print(" ");
  }

  for (int i = 0; i < LENGTH(buttons); i++) {
    bool state = !digitalRead(buttons[i].pin);
    if (state != buttons[i].lastState) {
      buttons[i].lastState = state;
      Serial.print("Button ");
      Serial.print(buttons[i].ID, HEX);
      Serial.print(" changed state to ");
      Serial.println(state);
      byte message[] = {0xf0, 0x03, 0x81, buttons[i].ID, state};
      ampSerial.write(message, sizeof(message));
    }
  }
}


void sendButtonEvent(byte ID, bool state) {
  byte message[] = {0xf0, 0x03, 0x81, ID, state};
  ampSerial.write(message, sizeof(message));
}
